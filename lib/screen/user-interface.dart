import 'package:amineware/controller/user-controller.dart';
import 'package:amineware/core/functions/validate-input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class UserInterface extends StatelessWidget {
  const UserInterface({super.key});

  @override
  Widget build(BuildContext context) {
    UserControllerImp controllerImp =
        Get.put(UserControllerImp());
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: controllerImp.formState,
        child: ListView(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              child: TextFormField(
                controller: controllerImp.text,
                validator: ((value) => validateInput(
                      value!,
                    )),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: "identification",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                onPressed: () {
                  controllerImp.addColumn(controllerImp.text.text);
                },
                color: Colors.black,
                textColor: Colors.white,
                padding: EdgeInsets.symmetric(vertical: 13),
                child: Text("Add"),
              ),
            ),
            GetBuilder<UserControllerImp>(
              builder: (controller) {
                if (controller.data.isEmpty) {
                  return Container();
                } else {
                  return Column(
                    children: [
                      Text("Data UnSorted",
                          style: TextStyle(fontSize: 30, color: Colors.blue)),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: controllerImp.data.length,
                          itemBuilder: (context, index) {
                            return Center(
                                child: Column(
                              children: [
                                Text(
                                  controllerImp.data[index],
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ));
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text("Data Order",
                          style: TextStyle(fontSize: 30, color: Colors.blue)),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 100,
                        child: ListView.separated(
                          shrinkWrap: true,
                          separatorBuilder: (context, index) => SizedBox(
                            width: 20,
                          ),
                          scrollDirection: Axis.horizontal,
                          itemCount: controllerImp.dataSorted.length,
                          itemBuilder: (context, index) {
                            return Text(
                              "${controllerImp.dataSorted.indexOf(controllerImp.data[index]) + 1} ",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                }
              },
            )
          ],
        ),
      ),
    ));
  }
}
