import 'package:alphanum_comparator/alphanum_comparator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

abstract class UserController extends GetxController {
  addColumn(String value);
}

class UserControllerImp extends UserController {
  TextEditingController text = TextEditingController();
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  List<String> data = [];
  List<String> dataSorted = [];

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    text.dispose();
  }

  @override
  addColumn(String value) {
    // TODO: implement addColumn
    if (formState.currentState!.validate()) {
      if (!data.contains(value)) {
        dataSorted.add(value);
        data.add(value);
        //  dataSorted.sort(AlphanumComparator.compare(value[0], value[0]));
        dataSorted.sort(AlphanumComparator.compareNumFirsts);
        text.clear();
        update();
      }
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    text = TextEditingController();
  }
}
